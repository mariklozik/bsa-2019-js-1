import View from './view';
import FightersView from './fightersView';
import App from './app';
import Fighter from './fighter';

class FighterModal extends View {
    constructor(fighter, fighterElement) {
        super();

        this.create(fighter);

        let closeButton = document.getElementById('close');
        closeButton.addEventListener('click', event => this.destroyModal(event), false);

        let updateButton = document.getElementById('update');
        updateButton.addEventListener('click', event => this.updateFighter(event, fighter, fighterElement), false);
    }

    create(fighter) {
        let
            name = this.createName(fighter.name),
            source = this.createImage(fighter.source),
            
            labelHealth = this.createLabel('Health', 'health'),
            inputHealth = this.createInput(fighter.health, 'health'),
            
            labelAttack = this.createLabel('Attack', 'attack'),
            inputAttack = this.createInput(fighter.attack, 'attack'),


            labelDefense = this.createLabel('Defense', 'defense'),
            inputDefense = this.createInput(fighter.defense, 'defense'),

            labelReady = this.createLabel('Ready to fight', 'isReady'),
            checkReady = this.createCheckbox(fighter._id, App.fightersReadyToFight),

            buttonSave = this.createButton('Save', 'update'),
            buttonClose = this.createButton('Close', 'close'),

            modal = this.createElement({
                tagName: 'div',
                className: 'modal',
                attributes: {id : 'modal'}
            }),
            modalContent = this.createElement({
                tagName: 'div',
                className: 'modal-content',                
            });

        modal.append(modalContent);
        modalContent.append(name, source, labelHealth, inputHealth, labelAttack,inputAttack, labelDefense, inputDefense, labelReady, checkReady, buttonSave, buttonClose);

        document.body.appendChild(modal);
    }

    createName(name) {
        const nameElement = this.createElement({
            tagName: 'h3',
            className: 'name'
        });
        nameElement.innerText = name;

        return nameElement;
    }

    createImage(source) {
        const attributes = {
            src: source
        };
        const imgElement = this.createElement({
            tagName: 'img',
            className: 'fighter-image',
            attributes
        });

        return imgElement;
    }

    createLabel(label, labelFor) {
        let labElement = this.createElement({
            tagName: 'label',
            className : 'modalLabel',
            attributes: {for : labelFor}
        });
        labElement.innerText = label;

        return labElement;
    }

    createInput(value, id = '') {
        let inpElement = this.createElement({
            tagName: 'input',
            className : 'props',
            attributes: { value : value, id : id }
        });

        return inpElement;
    }

    createCheckbox(fighterId, fighters) {
        let checkElement = this.createElement({
            tagName: 'input',
            className : 'modalInput',
            attributes: { type : 'checkbox', id : 'isReady'}
        });
        checkElement.checked = fighters.has(fighterId) ? true : false;

        return checkElement;
    }

    createButton(value, id = '') {
        let buttonElement = this.createElement({
            tagName: 'button',
            className : 'modalButton',
            attributes: {id : id}
        });
        buttonElement.innerHTML = value;

        return buttonElement;
    }

    updateFighter(event , fighter, fighterElement) {
        let properties = document.getElementsByClassName('props'),
            fighters   = App.fightersReadyToFight,
            isReady = document.getElementById('isReady').checked;

        for (let item of properties) {
            let property = item.id;
            fighter[property] = item.value;
        }

        if( isReady && fighters.size < 2 && !fighters.has(fighter._id) ) {
            fighters.set(fighter._id, new Fighter(fighter));
            fighterElement.classList.add('ready');
        }

        if(!isReady) {
            fighters.delete(fighter._id, fighter);
            fighterElement.classList.remove('ready');
        }

        this.destroyModal();
    }

    destroyModal(event) {
        let modal = document.getElementById('modal');
        modal.parentNode.removeChild(modal);
    }

}

export default FighterModal;