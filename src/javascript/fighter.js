class Fighter {
    constructor(fighter) {
        this._id = fighter._id;
        this.name = fighter.name;
        this.health = fighter.health;
        this.attack = fighter.attack;
        this.defense = fighter.defense;
        this.source = fighter.source;
    }

    getHitPower() {
        let 
            criticalHitChance = Math.round(Math.random() + 1),
            power = this.attack * criticalHitChance;

        return power;
    }

    getBlockPower() {
        let 
            dodgeChance = Math.round(Math.random() + 1),
            block = this.defense * dodgeChance;
            
        return block;
    }
}

export default Fighter;