import App from './app';
import View from './view';
import Fighter from './fighter';

class Arena extends View {
    constructor(fighters) {
        super();

        this.createFightButton('Fight!', 'fight');
        this.element.addEventListener('click', event => this.fight(), false);
    }

    createFightButton(value, id = '') {
        this.element = document.createElement('button');
        this.element.innerHTML = value;
        this.element.setAttribute('id', id);
    }

    fight() {
        let 
            fighters = Array.from(App.fightersReadyToFight.values()),
            isWinner = false;

        while( !isWinner ) {
            fighters[0].health = parseInt(fighters[0].health) + parseInt(fighters[0].getBlockPower()) - parseInt(fighters[1].getHitPower());
            fighters[1].health = parseInt(fighters[1].health) + parseInt(fighters[1].getBlockPower()) - parseInt(fighters[0].getHitPower());

            if(fighters[0].health <= 0) {            
                console.log(fighters[1].name + ' wins!');
                isWinner = true;
            }
            if(fighters[1].health <= 0) {
                console.log(fighters[0].name + ' wins!');
                isWinner = true;
            }          
        }
    

    }

}

export default Arena;