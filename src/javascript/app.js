import FightersView from './fightersView';
import { fighterService } from './services/fightersService';
import Arena from './arena';

class App {
    constructor() {
        this.startApp();
    }

    static rootElement = document.getElementById('root');
    static loadingElement = document.getElementById('loading-overlay');
    static fightersDetailsMap = new Map();
    static fightersReadyToFight = new Map();

    async startApp() {
        try {
            App.loadingElement.style.visibility = 'visible';

            const fighters = await fighterService.getFighters();
            const fightersView = new FightersView(fighters);
            const fightersElement = fightersView.element;
            const arena = new Arena();

            App.rootElement.appendChild(fightersElement);
            App.rootElement.appendChild(arena.element);
        } catch (error) {
            console.warn(error);
            App.rootElement.innerText = 'Failed to load data';
        } finally {
            App.loadingElement.style.visibility = 'hidden';
        }
    }
}

export default App;