import App from './app';
import View from './view';
import FighterView from './fighterView';
import { fighterService } from './services/fightersService';
import FighterModal from './fighterModal';
import Arena from './arena';

class FightersView extends View {
    constructor(fighters) {
        super();

        this.handleClick = this.handleFighterClick.bind(this);
        this.createFighters(fighters);
    }

    fightersDetailsMap = new Map();

    createFighters(fighters) {
        const fighterElements = fighters.map(fighter => {
            const fighterView = new FighterView(fighter, this.handleClick);
            return fighterView.element;
        });

        this.element = this.createElement({
            tagName: 'div',
            className: 'fighters'
        });
        this.element.append(...fighterElements);
    }

    async handleFighterClick(event, fighter, fighterElement) {
        if (!App.fightersDetailsMap.has(fighter._id)) {
            let fighterDetails = await fighterService.getFighterDetails(fighter._id)
            App.fightersDetailsMap.set(fighter._id, fighterDetails);
        }

        new FighterModal(App.fightersDetailsMap.get(fighter._id), fighterElement);
    }
}

export default FightersView;